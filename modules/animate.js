var typesOfAnim = {
  bounceInOut: 'bounceInOut',
  fadeOut: 'fadeOut',
  slideInOut: 'slideInOut',
  explodeOut: 'explodeOut'
},
animObject = {
  slideSize: '', // original slide size for explode effect
  type: '', // accepts fadeIn, slideIn, bounceIn
  speed: '', // speed of the animation
  slideTo: '', // amount of space the silde moves
  slideBounceTo: '', // amount of bouncing to animate
  slideExplodeTo: '', // amount of exploding effect to animate
  size: '', // total number of slides in array
  wrapper: '', // the animated slide parent

  setupAnimationBlock: function (settings) {
    for (option in settings) {
      this[option] = settings[option];
    }
  },

  anim: function (node, direction, callback) {
    if (!node) {
      throw new Error('Please indicate the node you wish to animate!');
      return;
    }
    // find the parent of the node, so appending and prepending works properly
    var wrapper = $(node).parent(),
      _this = this;

    switch (this.type) {
      case typesOfAnim.slideInOut:
      if (direction == 'next') {
        if ($(node).css('margin-left') > 0) $(node).css('margin-left', 0);

        $(node).animate({marginLeft: -this.slideTo }, this.speed, function () {
          if (callback && typeof callback == 'function') callback();
          $(node).appendTo(_this.wrapper).css('margin-left', 0);
        });
      }
      if (direction == 'previous') {
        $(node).css('margin-left', -this.slideTo).prependTo(wrapper).animate({ marginLeft: 0}, this.speed, function () {
          if (callback && typeof callback == 'function') callback();
        });
      }
      break;

      case typesOfAnim.fadeOut:
      if (direction == 'next') {
        $(node).fadeOut(this.speed, function () {
          if (callback && typeof callback == 'function') callback();
          $(node).css('display','block').prependTo(_this.wrapper).fadeIn(0);
        });
      }
      if (direction == 'previous') {
        $(node).css('display','none')
        .appendTo(wrapper)
        .fadeIn(this.speed, function () {
          if (callback && typeof callback == 'function') callback();
          $(node).css('display','block').appendTo(_this.wrapper).fadeIn(0);
        });
      }
      break;

      case typesOfAnim.bounceInOut:
      if (direction == 'next') {
        $(node).animate({ marginLeft: _this.slideBounceTo }, 500, function () {
          $(node).animate({ marginLeft: -_this.slideTo }, _this.speed, function () {
            if (callback && typeof callback == 'function') callback();
            $(node).appendTo(_this.wrapper).css('margin-left', 0);
          });
        });
      }
      if (direction == 'previous') {
        $(node).css('margin-left', -this.slideTo).prependTo(wrapper).animate({ marginLeft: 0 + _this.slideBounceTo}, this.speed, function () {
          $(node).animate({ marginLeft: 0}, 500, function () {
            if (callback && typeof callback == 'function') callback();
          });          
        });
      }
      break;

      case typesOfAnim.explodeOut:
      if (direction == 'next') {
        $(node).find('img').animate({
          width: $(node).width() * _this.slideExplodeTo,
          height: $(node).height() * _this.slideExplodeTo
        }, _this.speed);
        $(node).delay(100).fadeOut(_this.speed, function () {
          if (callback && typeof callback == 'function') callback();
          $(node).css('display','block').prependTo(_this.wrapper).fadeIn(0);
          $(node).find('img').css({ width: _this.slideSize.w, height: _this.slideSize.h });
        });
      }
      if (direction == 'previous') {        
        $(node).css('display','none')
        .appendTo(wrapper)
        .fadeIn(this.speed, function () {
          if (callback && typeof callback == 'function') callback();
        });
        $(node).find('img').css({
          width: _this.slideSize.w * _this.slideExplodeTo,
          height: _this.slideSize.h * _this.slideExplodeTo
        }).animate({
          width: _this.slideSize.w,
          height: _this.slideSize.h
        }, _this.speed);
      }
      break;
    }
  },

};