(function ($) {
  $.fn.inLoop = function (settings) {
    
    function app() {
      var totalWidth = 0,
      slideIndex = 1,
      timer,
      timeout,
      onAnimation = false,

      carouselWrapper,
      slideWrapper,
      paginateWrapper,
      controlWrapper,

      defaults = {
        slideSize: { w: 700, h: 400 },
        arrayOfSlides: [],
        autoPlayTimeout: 3000,
        autoPlayInterval: 2000,
        animationSpeed: 1000,
        animationType: 'slideIn' // accepts fadeIn, slideIn
      };

      return {
        init: function (settings) {
          // if user defines their own settings
          // then we merge theirs with our defaults
          if (settings && typeof settings == 'object') {
            defaults = jQuery.extend(defaults, settings);
          }
          // bring in the modules
          this.setupModules();          
          slideWrapper = (defaults.slideshowId) ? $('ul#'+defaults.slideshowId) : $('ul#gallery'),
          carouselWrapper = $(slideWrapper).parent(),
          paginateWrapper = $(carouselWrapper).find('div.bottom-wrapper div.paginate-wrapper ul.page-wrapper'),
          controlWrapper = $(carouselWrapper).find('div#controller');

          // prepare animation block
          this.animation.setupAnimationBlock({
            slideSize: { w: 700, h: 400 },
            type: defaults.animationType,
            speed: (defaults.animationSpeed) ? defaults.animationSpeed : 1000,
            offset: defaults.slideSize.w,
            size: defaults.arrayOfSlides.length,
            wrapper: slideWrapper,

            slideTo: (defaults.slideTo) ? defaults.slideTo : defaults.slideSize.w,
            slideBounceTo: (defaults.slideBounceTo) ? defaults.slideBounceTo : 50,
            slideExplodeTo: (defaults.slideExplodeTo) ? defaults.slideExplodeTo : 1.5,
          });

          this.setupCarousel();
        },

        setupModules: function () {
          this['animation'] = jQuery.extend({}, animObject);
        },

        setupCarousel: function () {
          var _this = this;
          totalWidth = defaults.slideSize.w * defaults.arrayOfSlides.length;

          slideWrapper.css('width', totalWidth);
          if (defaults.arrayOfSlides.length > 0) {
            this.createSlides();        
            // if there's only 1 slide in the array
            // we stop the rest of the process
            if (defaults.arrayOfSlides.length == 1) return;
            this.createPagination();
            this.controllerClicked();
            timer = setInterval(function () { _this.autoRotateSlides() }, defaults.autoPlayInterval);        
          }          
        },

        createSlides: function () {
          // setup all the slides base on the animation
          // arrangement of slides can differ due to animation techniques
          switch (defaults.animationType) {
            case typesOfAnim.slideInOut:
            case typesOfAnim.bounceInOut:
            for (var i = 0; i < defaults.arrayOfSlides.length; i++) {
              slideWrapper.append('<li id="'+ defaults.slideshowId + (i + 1) +'" class="slides slideIn"><img src="'+ defaults.arrayOfSlides[i] +'" alt=""></li>');
            }
            break;
            case typesOfAnim.fadeOut:
            case typesOfAnim.explodeOut:
            for (var i = (defaults.arrayOfSlides.length - 1); i > -1; i--) {
              $('<li id="'+ defaults.slideshowId + (i + 1) +'" class="slides fadeIn"></li>')
              .append('<img src="'+ defaults.arrayOfSlides[i] +'" alt="">')
              .appendTo(slideWrapper);
            }
            break;
            // for single image carousel
            default:
            $('<li id="1" class="slides fadeIn"></li>')
              .append('<img src="'+ defaults.arrayOfSlides[0] +'" alt="">')
              .appendTo(slideWrapper);
            break;
          }
        },

        createPagination: function () {
          for (var i = 1; i <= defaults.arrayOfSlides.length; i++) {
            $(paginateWrapper).append('<li id="'+i+'" class="page"><a href="#slide'+i+'"></a></li>');
          }
          this.paginationClicked();
          this.onChangePage();
        },

        autoRotateSlides: function () {
          var _this = this,
              slide = slideWrapper.find('li.slides#'+ defaults.slideshowId + slideIndex);
          // set flag to indicate it's animating, so it doesn't conflict
          onAnimation = true;
          // using animation module to handle/perform animation,
          // we provide a callback function to handle next event
          this.animation.anim(slide, 'next', function () {            
            if (slideIndex == defaults.arrayOfSlides.length) {
              slideIndex = 1;
            } else {
              slideIndex++;
            }
            _this.onChangePage();
            onAnimation = false;
          });
        },

        onChangePage: function () {
          // reset all active states on all pages
          paginateWrapper.find('li.page').find('a').removeClass('active');
          // change pages base on the slide index user browser to
          var current = paginateWrapper.find('li.page')[slideIndex - 1];
          $(current).find('a').addClass('active');
        },

        onChangeSlide: function (previous) {
          var index, slide, _this = this;
          // prepend the selected slide first, and animate back
          if (previous) {
            index = slideIndex - 1;
            if (index > 0) {
              slide = slideWrapper.find('li.slides#'+ defaults.slideshowId + index);
              slideIndex--; // moving back one step at a time in the array
            } else {
              slideIndex = defaults.arrayOfSlides.length;
              index = slideIndex; // reseting the index back to the last array index
              // line repeated for this case because we need to jump to the end of the array
              slide = slideWrapper.find('li.slides#'+ defaults.slideshowId + index);
            }
            // perform animation            
            this.animation.anim(slide, 'previous', function () { _this.onChangePage(); });
          } else {
            index = slideIndex;
            slide = slideWrapper.find('li.slides#'+ defaults.slideshowId + index);
            if (index < defaults.arrayOfSlides.length) {
              slideIndex++; // moving forward one step at a time in the array
            } else if (index == defaults.arrayOfSlides.length) {
              slideIndex = 1; // if user has reached the end of the array, we jump back to the start of the array
            }
            // perform animation
            this.animation.anim(slide, 'next', function () { _this.onChangePage(); });
          }
          this.resetTimers();// we always assume to reset timers
          // set timeout if user doesn't interact, resume auto play
          timeout = setTimeout(function () {
            timer = setInterval(function () { _this.autoRotateSlides() }, defaults.autoPlayInterval);
          }, defaults.autoPlayTimeout);
        },

        controllerClicked: function () {
          var _this = this;
          controlWrapper.find('a.slide-controller').on('click', function (e) {
            // if carousel is on animation, we ignore clicks
            if (onAnimation) return;        
            _this.resetTimers(); // always assume to reset all timers
            // to determine if action is moving forward or backward
            if (e.target.id == 'nextSlide') _this.onChangeSlide(); else _this.onChangeSlide(true);
          });
        },

        paginationClicked: function () {
          var _this = this;
          paginateWrapper.find('li.page').on('click', function (e) {_this.resetTimers(); });
        },

        resetTimers: function () {
          clearTimeout(timeout);
          clearInterval(timer);
        },

        cons: function () { console.log('test: '+settings.slideshowId) }
      };
    }

    var demo = new app();
    // automatically initialize the plugin
    demo.init(settings || '');
    demo.cons();
    
    return demo;
  };
})(jQuery);